import { Component, Inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ImagePreviewComponent } from '@rhinov/delivering/ui/ui-image-preview';
import {
  PROMOTIONAL_IMAGES_QUERY_TOKEN,
  GO_TO_ADVERTISING_WINDOW_COMMAND_TOKEN,
  AdvertiseCard,
  PromotionalImagesLanding,
} from '@rhinov/promoting/application';
import {
  INSPIRATION_PHOTO_URLS_QUERY,
  INSPIRATION_IMAGE_URLS_QUERY,
  ROOM_FEATURES_QUERY,
  PROJECT_BRIEF_QUERY,
  RoomFeature,
  ProjectBrief,
} from '@rhinov/delivering/application';

import { Observable } from 'rxjs';
import { Command, Query } from '@rhinov/shared/cqrx';
import { AdvertisingCardData } from '@rhinov/shared/application';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

@Component({
  selector: 'rhinov-project-brief-desktop',
  templateUrl: './project-brief-desktop.component.html',
  styleUrls: ['./project-brief-desktop.component.scss'],
})
export class ProjectBriefDesktopComponent {
  advertiseCards$: Observable<AdvertiseCard[]>;
  inspirationPhotos$: Observable<string[]>;
  inspirationImages$: Observable<string[]>;
  roomFeatures$: Observable<RoomFeature[]>;
  projectBrief$: Observable<ProjectBrief>;

  swiperConfig: SwiperConfigInterface = {
    slidesPerView: 'auto',
    spaceBetween: 8,
    loop: false,
    centerInsufficientSlides: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
      type: 'bullets',
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  };

  private readonly galleryPopUpWidth = 944;
  private readonly galleryPopUpMaxHeight = 903;
  private readonly galleryPopUpContentPadding = 200;

  constructor(
    @Inject(PROMOTIONAL_IMAGES_QUERY_TOKEN)
    private advertiseCardsQuery: Query<AdvertiseCard[], PromotionalImagesLanding>,
    @Inject(GO_TO_ADVERTISING_WINDOW_COMMAND_TOKEN)
    private openAdvertising: Command<AdvertiseCard, boolean>,
    @Inject(INSPIRATION_PHOTO_URLS_QUERY) private inspirationPhotoUrlsQuery: Query<string[]>,
    @Inject(INSPIRATION_IMAGE_URLS_QUERY) private inspirationImageUrlsQuery: Query<string[]>,
    @Inject(ROOM_FEATURES_QUERY) private roomFeaturesQuery: Query<RoomFeature[]>,
    @Inject(PROJECT_BRIEF_QUERY) private projectBriefQuery: Query<ProjectBrief>,
    public dialog: MatDialog
  ) {
    this.advertiseCards$ = this.advertiseCardsQuery.values$;
    this.inspirationPhotos$ = this.inspirationPhotoUrlsQuery.values$;
    this.inspirationImages$ = this.inspirationImageUrlsQuery.values$;
    this.roomFeatures$ = this.roomFeaturesQuery.values$;
    this.projectBrief$ = this.projectBriefQuery.values$;

    this.advertiseCardsQuery.execute('project-brief');
  }

  onAdvertisingCardClick(card: AdvertisingCardData) {
    const adCard = {
      linkText: card.urlLink,
    } as AdvertiseCard;
    this.openAdvertising.execute(adCard);
  }

  convertAdCard(card: AdvertiseCard): AdvertisingCardData {
    return {
      id: '',
      imageSmall: card.backgroundImage,
      imageLarge: card.backgroundImageLarge,
      urlLink: card.linkText,
      imageSmallAdaptive: card.backgroundImageAdaptive,
      imageLargeAdaptive: card.backgroundImageLargeAdaptive,
    } as AdvertisingCardData;
  }

  openGalleryPopUp(images: string[], startFromImageWithIndex: number): void {
    this.dialog.open(ImagePreviewComponent, {
      width: `${this.galleryPopUpWidth}px`,
      maxWidth: '100vw',
      maxHeight: `${this.galleryPopUpMaxHeight}px`,
      panelClass: 'image-preview-dialog-position',
      data: {
        images,
        startFromImageWithIndex,
        maxHeight: this.galleryPopUpMaxHeight,
        contentPadding: this.galleryPopUpContentPadding,
      },
    });
  }
}

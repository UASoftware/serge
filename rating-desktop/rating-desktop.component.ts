import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EVALUATE_PROJECT_COMMAND_TOKEN, Evaluation } from '@rhinov/evaluating/application';
import { Command } from '@rhinov/shared/cqrx';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'rhinov-rating-desktop',
  templateUrl: './rating-desktop.component.html',
  styleUrls: ['./rating-desktop.component.scss'],
})
export class RatingDesktopComponent {
  private projectRating = 0;
  private designerRating = 0;
  private appRating = -1;
  private appHover: number | null = null;
  private allowShare = false;
  comments = '';

  constructor(
    public dialogRef: MatDialogRef<RatingDesktopComponent>,
    @Inject(EVALUATE_PROJECT_COMMAND_TOKEN)
    private sendProjectEvaluation: Command<Evaluation, boolean>,
    @Inject(MAT_DIALOG_DATA) public dialogWasOpenedByTimeout?: boolean
  ) {}

  updateProjectRating(rating: number) {
    this.projectRating = rating;
  }

  updateDesignerRating(rating: number) {
    this.designerRating = rating;
  }

  updateAppRating(rating: number) {
    this.appRating = rating;
  }

  hoverAppRating(rating: number) {
    this.appHover = rating;
  }

  hideHover() {
    this.appHover = null;
  }

  setShareState(state: boolean) {
    this.allowShare = state;
  }

  getAppRating(): number {
    return this.appRating;
  }

  getAppHover(): number | null {
    return this.appHover;
  }

  get canDisplayShare(): Observable<boolean> {
    return of(this.projectRating > 3 && this.designerRating > 3);
  }

  onClose() {
    this.dialogRef.close();
  }

  get isSubmitDisabled() {
    return this.projectRating === 0 || this.designerRating === 0 || this.appRating < 0;
  }

  onSubmit() {
    const evaluation: Evaluation = {
      template: this.projectRating,
      decorator: this.designerRating,
      application: this.appRating,
      publishPhotos: this.allowShare,
      comments: this.comments,
    };

    this.dialogRef.close();

    this.sendProjectEvaluation.execute(evaluation);
  }
}

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { ClickEvent, HoverRatingChangeEvent } from 'angular-star-rating';

@Component({
  selector: 'rhinov-stars-rating',
  templateUrl: './stars-rating.component.html',
  styleUrls: ['./stars-rating.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StarsRatingComponent {
  @Input() rating = 0;
  @Input() message!: string;
  @Output() clickOnStars = new EventEmitter<number>();

  evaluationHover: number | null = null;

  onHoverRatingChange(event: HoverRatingChangeEvent) {
    this.evaluationHover = event.hoverRating;
  }

  starSelected(event: ClickEvent) {
    if (this.rating !== null && this.rating !== undefined) {
      this.rating = event.rating;
      this.clickOnStars.emit(this.rating);
    }
  }
}

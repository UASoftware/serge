import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { Command, Query } from '@rhinov/shared/cqrx';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

import {
  GO_TO_PROJECT_COMMAND_TOKEN,
  GO_TO_INTERIOR_COMMAND_TOKEN,
  GO_TO_SHOPPING_COMMAND_TOKEN,
  NAV_BAR_ACTIVE_TAB_QUERY_TOKEN,
  ProjectCardData,
  NavBarTabs,
} from '@rhinov/delivering/application';
import { UserAccess, USER_ACCESS_QUERY_TOKEN } from '@rhinov/users/application';
import { GO_TO_ARTICLES_COMMAND_TOKEN } from '@rhinov/articles/application';
import { APPLICATION_NAME_QUERY_TOKEN } from '@rhinov/shared/application';
import { ApplicationName } from '@rhinov/shared/environments';
import { IS_ROOM_TYPE_EXTERIOR_QUERY_TOKEN } from '@rhinov/medias/application';

@UntilDestroy()
@Component({
  selector: 'rhinov-nav-bar-desktop',
  templateUrl: './nav-bar-desktop.component.html',
  styleUrls: ['./nav-bar-desktop.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavBarDesktopComponent {
  NavBarTabs = NavBarTabs;

  navBarGroup: FormGroup = new FormGroup({
    menu: new FormControl(NavBarTabs.Project, Validators.required),
  });
  isRoomTypeExterior$ = this.isRoomTypeExteriorQuery.values$;
  userAccess$ = this.userAccessQuery.values$;
  applicationType$ = this.applicationNameQuery.values$;

  constructor(
    @Inject(IS_ROOM_TYPE_EXTERIOR_QUERY_TOKEN) private isRoomTypeExteriorQuery: Query<boolean>,
    @Inject(GO_TO_PROJECT_COMMAND_TOKEN)
    private goToProjectCommand: Command<ProjectCardData | undefined, boolean>,
    @Inject(GO_TO_INTERIOR_COMMAND_TOKEN) private goToInteriorCommand: Command<undefined, boolean>,
    @Inject(GO_TO_SHOPPING_COMMAND_TOKEN) private goToShopCommand: Command<undefined, boolean>,
    @Inject(GO_TO_ARTICLES_COMMAND_TOKEN) private goToArticlesCommand: Command<undefined, boolean>,
    @Inject(NAV_BAR_ACTIVE_TAB_QUERY_TOKEN) private activeNavBarTabQuery: Query<NavBarTabs>,
    @Inject(USER_ACCESS_QUERY_TOKEN) private userAccessQuery: Query<UserAccess | undefined>,
    @Inject(APPLICATION_NAME_QUERY_TOKEN) private applicationNameQuery: Query<ApplicationName>
  ) {
    this.activeNavBarTabQuery.values$.pipe(untilDestroyed(this)).subscribe((tabID) => {
      this.navBarGroup.setValue({
        menu: tabID,
      });
    });
  }

  onProjectClick(data: Event) {
    this.goToProjectCommand.execute();
  }

  onInteriorClick(data: Event) {
    this.goToInteriorCommand.execute();
  }

  onShopClick(data: Event) {
    this.goToShopCommand.execute();
  }

  onArticlesClick(data: Event) {
    this.goToArticlesCommand.execute();
  }
}

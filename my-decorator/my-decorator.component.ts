import { Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';

import { Query, Command } from '@rhinov/shared/cqrx';
import {
  AdvertiseCard,
  PROMOTIONAL_IMAGES_QUERY_TOKEN,
  GO_TO_ADVERTISING_WINDOW_COMMAND_TOKEN,
  PromotionalImagesLanding,
} from '@rhinov/promoting/application';
import { Designer, DESIGNER_QUERY_TOKEN } from '@rhinov/delivering/application';
import { AdvertisingCardData } from '@rhinov/shared/application';
import { map } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'rhinov-my-decorator',
  templateUrl: './my-decorator.component.html',
  styleUrls: ['./my-decorator.component.scss'],
})
export class MyDecoratorComponent {
  private readonly style = `
    @font-face {
      font-family: 'fabrikat-regular';
      src: url('${location.origin}/assets/fonts/fabrikat-regular-webfont.woff2') format('woff2'),
        url('${location.origin}/assets/fonts/fabrikat-regular-webfont.woff') format('woff');
      font-weight: 400;
      font-style: normal;
    }

    p, a {
      font-family: 'fabrikat-regular';
      text-align: left;
      font-size: 17px;
      line-height: 24px;
      word-wrap: normal;
      white-space: pre-line;
    }

    p {
      color: rgba(61, 60, 64, 0.7);
      margin: 0 0 24px 0;
    }

    p:last-child {
      margin: 0;
    }`;
  @ViewChild('frame') frame?: ElementRef<HTMLIFrameElement>;

  decoratorInfo$: Observable<Designer>;
  advertisingCards$: Observable<AdvertiseCard[]>;
  frameContent$ = this.requestDecoratorInfo.values$.pipe(
    map((data) => {
      const html = `
      <html>
        <head>
          <style>
            ${this.style}
          </style>
        </head>
        <body>
          ${data.comments}
        </body>
      </html>
    `;

      const blob = new Blob([html], { type: 'text/html' });
      const url = URL.createObjectURL(blob);
      return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    })
  );

  constructor(
    @Inject(DESIGNER_QUERY_TOKEN)
    private requestDecoratorInfo: Query<Designer>,
    @Inject(PROMOTIONAL_IMAGES_QUERY_TOKEN)
    private requestAdvertisingCards: Query<AdvertiseCard[], PromotionalImagesLanding>,
    @Inject(GO_TO_ADVERTISING_WINDOW_COMMAND_TOKEN)
    private openAdvertising: Command<AdvertiseCard, boolean>,
    private sanitizer: DomSanitizer
  ) {
    this.decoratorInfo$ = this.requestDecoratorInfo.values$;
    this.advertisingCards$ = this.requestAdvertisingCards.values$;
    this.requestAdvertisingCards.execute('project-designer');
  }

  updateFrameHeight() {
    if (this.frame?.nativeElement) {
      this.frame.nativeElement.height =
        this.frame.nativeElement.contentDocument!.body.scrollHeight + 'px';
    }
  }

  onAdvertisingCardClick(advertiseCard: AdvertiseCard) {
    this.openAdvertising.execute(advertiseCard);
  }

  convertAdCard(card: AdvertiseCard): AdvertisingCardData {
    return {
      id: '',
      imageSmall: card.backgroundImage,
      imageLarge: card.backgroundImageLarge,
      urlLink: card.linkText,
      imageSmallAdaptive: card.backgroundImageAdaptive,
      imageLargeAdaptive: card.backgroundImageLargeAdaptive,
    } as AdvertisingCardData;
  }
}

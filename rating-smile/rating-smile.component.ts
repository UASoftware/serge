import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

interface SmileIconSettings {
  picto: string;
  text: string;
  class: string;
}

enum SmileIconIndex {
  Default = 0,
  VeryBad = 1,
  Bad = 2,
  Ok = 3,
  Happy = 4,
  Love = 5,
}

@Component({
  selector: 'rhinov-rating-smile',
  templateUrl: './rating-smile.component.html',
  styleUrls: ['./rating-smile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RatingSmileComponent {
  @Input() rating!: number;
  @Input() evaluationHover: number | null = null;
  @Input() isAppRating: boolean | null = false;

  private smileName: SmileIconSettings[] = [
    {
      picto: 'neutral',
      text: ' ',
      class: 'grey',
    },
    {
      picto: 'verybad',
      text: 'Très insatisfait(e)',
      class: 'red',
    },
    {
      picto: 'bad',
      text: 'Pas satisfait(e)',
      class: 'orange',
    },
    {
      picto: 'ok',
      text: 'Bof bof',
      class: 'yellow',
    },
    {
      picto: 'happy',
      text: 'Satisfait(e)',
      class: 'blue',
    },
    {
      picto: 'love',
      text: 'J’adore',
      class: 'green',
    },
  ];

  private getAppSmileName(rating: number) {
    switch (rating) {
      case 0:
      case 1:
      case 2:
        return this.smileName[SmileIconIndex.VeryBad];
      case 3:
      case 4:
        return this.smileName[SmileIconIndex.Bad];
      case 5:
      case 6:
        return this.smileName[SmileIconIndex.Ok];
      case 7:
      case 8:
        return this.smileName[SmileIconIndex.Happy];
      case 9:
      case 10:
        return this.smileName[SmileIconIndex.Love];
      default:
        return this.smileName[SmileIconIndex.Default];
    }
  }

  private getSmile(): SmileIconSettings {
    if (this.isAppRating) {
      if (this.evaluationHover != null) {
        return this.getAppSmileName(this.evaluationHover);
      } else {
        return this.getAppSmileName(this.rating);
      }
    } else {
      if (this.evaluationHover) {
        return this.smileName[this.evaluationHover];
      } else {
        return this.smileName[this.rating];
      }
    }
  }

  getSmileImage(): string {
    return this.getSmile().picto;
  }

  getClassImage(): string {
    return this.getSmile().class;
  }

  getTextImage(): string {
    return this.getSmile().text;
  }
}
